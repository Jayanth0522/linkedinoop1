package com.innominds.mar2022.abstracts;

import com.innominds.mar2022.dependencies.Job;
import com.innominds.mar2022.interfaces.IAccount;
/**
 * Implementation BasicUser class inheritates form BasicAccount
 * @author jpalepally1
 *
 */
public abstract class AAccount implements IAccount  {
	
	protected Job[] previousJobs;
	
	public abstract int getAccountNumber();

	/**
	 * Getter for the set of previous jobs
	 * @return
	 */
	public Job[] getPreviousJobs() {
		return previousJobs;
	}
}
