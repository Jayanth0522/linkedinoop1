package com.innominds.mar2022.dependancies;

/**
 * Implementation of Address Class
 * 
 * @author jpalepally1
 */
public class Address {
	/**
	 * Implementation of Instance Variables
	 */

	private String HouseNo;
	private String Street;
	private String Landmark;
	private String District;
	private String State;
	private String Country;

	/**
	 * Constructor for the Address Class
	 * 
	 * @param houseNo
	 * @param street
	 * @param landmark
	 * @param district
	 * @param state
	 * @param country
	 */
	public Address(String houseNo, String street, String landmark, String district, String state, String country) {

		this.HouseNo = houseNo;
		this.Street = street;
		this.Landmark = landmark;
		this.District = district;
		this.State = state;
		this.Country = country;

	}

	/**
	 * Using ToSString Method to show the output in a Human readable form
	 */
//	@Override
	public String toString() {
		// concatenating various parts of the address

		String returnString = "";
		returnString += "HouseNo   :" + this.HouseNo + "\n";
		returnString += "Street     :" + this.Street + "\n";
		returnString += "Landmark   :" + this.Landmark + "\n";
		returnString += "District   :" + this.District + "\n";
		returnString += "State      :" + this.State + "\n";
		returnString += "Country    :" + this.Country + "\n";

		return returnString;
	}
}
