package com.innominds.mar2022.classes;

import java.util.Date;

import com.innominds.mar2022.abstracts.AAccount;
import com.innominds.mar2022.dependancies.Address;

/**
 * Implementation of basic account class.
 * 
 * @author jpalepally1
 */
public class BasicAccount extends AAccount {

	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private String phoneNo;
	private Date dob;
	private String[] qualification;
	private String[] skills;
	private int accountnumber;
	private String[] Job;

	/**
	 * Constructor
	 * 
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param password
	 * @param phoneNo
	 * @param qualification
	 * @param skills
	 * @param addr
	 * @param accountnumber
	 */

	public BasicAccount(String firstname, String lastname, String email, String password, String phoneNo, Date dob,
			String qualification[], String skills[], int accountnumber, String[] jobs) {

		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.phoneNo = phoneNo;
		this.dob = dob;
		this.qualification = qualification;
		this.skills = skills;
		this.accountnumber = accountnumber;
		this.Job = jobs;

	}

	@Override
	public String getFirstName() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLastName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getEmailAddress() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPhoneNo() {
		// TODO Auto-generated method stub
		return null;
	}

	public String[] getQualification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getSkills() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public Date getDob() {
//		// TODO Auto-generated method stub
//		return null;
//	}

//	@Override
//	public Address getAddr() {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public int getAccountNumber() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Date getDob() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address getAddr() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Using the to string method to return the Details of the user
	 */

	public String toString() {
		String details = " ";
		details += "FirstName  :" + this.firstname + "\n";
		details += "LastName  :" + this.lastname + "\n";
		details += "Email  :" + this.email + "\n";
		details += "Password  :" + this.password + "\n";
		details += "Phonenumber  :" + this.phoneNo + "\n";
		details += "dob  :" + this.dob + "\n";

		/**
		 * Using the For loop to print the arrays of Qualification,skills,Jobs
		 */

		String qualstring = "[";
		for (int i = 0; i < this.qualification.length; i++) {
			qualstring += "\"" + this.qualification[i] + "\" ";
		}
		qualstring += "]";
		details += "qualification: " + qualstring + "\n";

		String skillstring = "[";
		for (int i = 0; i < this.skills.length; i++) {
			skillstring += "\"" + this.skills[i] + "\" ";
		}
		skillstring += "]";
		details += "Skills: " + skillstring + "\n";

		details += "AccNo  :" + this.accountnumber + "\n";

		String jobsstring = "[";
		for (int i = 0; i < this.Job.length; i++) {
			jobsstring += "\"" + this.Job[i] + "\" ";
		}
		jobsstring += "]";
		details += "Jobs: " + jobsstring + "\n";
		return details;
	}

}
