package com.innominds.mar2022.interfaces;
/**
 * This is the implementation of Interface of the Account
 * 
 */
import java.util.Date;

import com.innominds.mar2022.dependancies.Address;

public interface IAccount {
		
		/**
		 * Getter for the string First name
		 * @return First name instance variable
		 */
		public String getFirstName();
		
		/**
		 * getter for the string emailAddress
		 * @return emailAddress
		 */
		
		public String getEmailAddress();
		
		/**
		 * getter for the list of skills
		 * @return an array of strings of skills
		 */
		public String[] getSkills();
		
		/**
		 * getter for the Date object representing date of birth
		 * @return java.util.Date object representing date of birth of user
		 */
		public Date getDob();
		
		/**
		 * getter for the address object
		 * @return Address type, representing the address of the user
		 */
		public Address getAddr();
		
		/**
		 * getter for the account number of user
		 * @return an account number of the user, long
		 */
}

