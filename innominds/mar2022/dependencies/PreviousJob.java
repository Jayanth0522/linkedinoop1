package com.innominds.mar2022.dependencies;



/**
 * This is the class to implement the Previous jobs of the user
 * @author jpalepally1
 *
 */
public class PreviousJob {
	private String CompanyName;
	private String JobRole;
	private int Yearsofexperience;
	
	/**
	 * Creating the constructor for the Previous Job class
	 * @param companyName
	 * @param jobRole
	 * @param startdate
	 * @param enddate
	 * @param yearsofexperience
	 */
	public PreviousJob(String companyName, String jobRole, int yearsofexperience) {
		
	this.CompanyName = companyName;
	this.JobRole = jobRole;
	this.Yearsofexperience = yearsofexperience;
	}
	/**
	 * Using the toString method to return the given parameters
	 */
	
	public String toString() {
		String details ="";
		
		details += "CompanyName:       "+ this.CompanyName +"\n";
		details += "JobRole        	   "+ this.JobRole     +"\n";
		details += "Yearsofexperience  "+ this.Yearsofexperience +"\n";
				
		return details ;		
	}
}
