package com.innominds.mar2022.dependencies;
/**
 * This is the implementation of the Job details of the user 
 */

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class Job {
	/**
	 * Declaring the instance variables
	 */
	private String Designation;
	private Date startdate;
	private Date enddate;
	
	/**
	 * Creating the Constructor for the Job class
	 * @param Designation
	 * @param startdate
	 * @param enddate
	 */
	public Job(String Designation,Date startdate,Date enddate){
		this.Designation = Designation;
		this.startdate   = startdate;
		this.enddate     = enddate;			
	}
	/**
	 * Using the to string method to return the designation,Joining date and end date 
	 * of the user 
	 */
	
	@Override
	public String toString() {
		String str = "";
		LocalDate startDateAsLocalDate = this.startdate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		
		str += this.Designation + "\n";
		str += "from " + startDateAsLocalDate.getMonth() + ", " + startDateAsLocalDate.getYear() + "\n";
		if (this.enddate != null) {
			LocalDate endDateAsLocalDate = this.enddate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			str += "to " + endDateAsLocalDate.getMonth() + ", " + endDateAsLocalDate.getYear();
		} else {
			str += "currently working";
		}
		
		return str;
	}
	
	
}
	
