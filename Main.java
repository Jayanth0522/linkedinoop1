package com;

/**
 * This is the implementation of the main method 
 * @author jpalepally1
 */

import java.util.Date;

import com.innominds.mar2022.classes.BasicAccount;
import com.innominds.mar2022.dependancies.Address;
import com.innominds.mar2022.dependencies.Job;
import com.innominds.mar2022.dependencies.PreviousJob;

public class Main {

	public static void main(String[] args) {	
		
		/**
		 * Simple test for the details of the user
		 */
		
		System.out.println("User Details");
		System.out.println("============");

		@SuppressWarnings("deprecation")
		BasicAccount Base = new BasicAccount("Jayanth", "Kumar", "Jai@123", "Sai@234", "9853737373",
				new Date("12/05/1998"), new String[] { "BTech", "Mtech" }, new String[] { "Java", "Sql", "Python" },
				12345667, new String[] { "Support role", "Devoloper" });
		System.out.println(Base);
		/**
		 * Simple tester for the Address Object
		 */

		Address add = new Address("20-54", "Annapurna colony", "Baba Temple", "Hyderabad", "Telangana", "India");
		System.out.println("   Address   ");
		System.out.println("=============");
		System.out.println(add);

		/**
		 * Simple tester for the Job object
		 * 
		 */

		@SuppressWarnings("deprecation")
		
		Date dstart = new Date(120, 2, 15);
		@SuppressWarnings("deprecation")
		Date dend = new Date(122, 5, 24);
		System.out.println("Designation Details");
		System.out.println("====================");
		Job j = new Job("Associate Trainee", dstart, dend);
		System.out.println(j);
				
		
		/**
		 * Calling the class of the previousJob and their parameters
		 */
		
		System.out.println("Details of the previous work Experience of the User");
		System.out.println("===================================================");
		PreviousJob  Job = new PreviousJob("Tata Consultancy Services", "Support Role",01 );
		System.out.println(Job);
		
		
		
		
		
		
		
	}
}
